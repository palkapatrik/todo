export enum ListActions {
  ARCHIVE,
  REVIVE,
  DELETE,
  RENAME
}
