import {environment} from '../../environments/environment';

export class ApiRoutes {
  static TASKS = `${environment.api}/tasks`;
  static LISTS = `${environment.api}/lists`;
}
