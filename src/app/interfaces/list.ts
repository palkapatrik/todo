export interface List {
  id: string;
  name: string;
  archived: boolean;
}
