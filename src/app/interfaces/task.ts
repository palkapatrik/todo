export interface Task {
  id: string;
  value: string;
  listId: string;
  completed: boolean;
}
