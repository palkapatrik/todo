import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListComponent } from './components/list/list.component';
import { BoardComponent } from './components/board/board.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import {CardModule} from 'primeng/card';
import { NavigationBarComponent } from './components/navigation-bar/navigation-bar.component';
import { SpinnerComponent } from './components/spinner/spinner.component';
import {MenubarModule} from 'primeng/menubar';
import { ModalDialogComponent } from './components/modal-dialog/modal-dialog.component';
import {DialogModule} from 'primeng/dialog';
import {InputTextModule} from 'primeng/inputtext';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ButtonModule} from 'primeng/button';
import {NewTaskComponent} from './components/new-task/new-task.component';
import { ArchivePageComponent } from './pages/archive-page/archive-page.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { ListActionButtonsComponent } from './components/list-action-buttons/list-action-buttons.component';
import {TooltipModule} from 'primeng/tooltip';
import {InplaceModule} from 'primeng/inplace';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    BoardComponent,
    ListItemComponent,
    NavigationBarComponent,
    SpinnerComponent,
    ModalDialogComponent,
    NewTaskComponent,
    ArchivePageComponent,
    HomePageComponent,
    ListActionButtonsComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    CardModule,
    MenubarModule,
    DialogModule,
    InputTextModule,
    FormsModule,
    ButtonModule,
    TooltipModule,
    InplaceModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
