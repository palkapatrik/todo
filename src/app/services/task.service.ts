import {Injectable} from '@angular/core';
import {Task} from '../interfaces/task';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {CommonService} from './common.service';
import {ApiRoutes} from '../enums/api-routes';

@Injectable({
  providedIn: 'root'
})
export class TaskService {

  addTask(value, listId) {
    const newTask: Task = {
      id: this.commonService.generateId(),
      value,
      listId,
      completed: false
    };
    return this.http.post<Task>(`${ApiRoutes.TASKS}/${newTask.listId}/add`, newTask);
  }

  removeTask(id): Observable<any> {
    return this.http.delete(`${ApiRoutes.TASKS}/delete/${id}`);
  }

  toggleTaskState(task: Task, completed: boolean): Observable<Task> {
    task.completed = completed;
    return this.http.put<Task>(`${ApiRoutes.TASKS}/${task.id}`, task);
  }

  getTasks(listId): Observable<Task[]> {
    return this.http.get<Task[]>(`${ApiRoutes.TASKS}/${listId}`);
  }

  updateTask(task: Task) {
    return this.http.put<Task>(`${ApiRoutes.TASKS}/update/${task.id}`, task);
  }

  /*
    getTask(id): Observable<Task> {
      return this.http.get<Task>(`${ApiRoutes.TASKS}/single/${id}`);
    }
  */
  changeTaskValue(task: Task, newVal: string): Observable<Task> {
    task.value = newVal;
    return this.http.put<Task>(`${ApiRoutes.TASKS}/${task.id}`, task);
  }

  constructor(private http: HttpClient,
              private commonService: CommonService) {
  }
}
