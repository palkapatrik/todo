import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {List} from '../interfaces/list';
import {HttpClient} from '@angular/common/http';
import {CommonService} from './common.service';
import {ApiRoutes} from '../enums/api-routes';

@Injectable({
  providedIn: 'root'
})
export class ListService {

  getLists(archived: boolean = false): Observable<List[]> {
    if (archived) {
      return this.http.get<List[]>(`${ApiRoutes.LISTS}/archived`);
    }
    return this.http.get<List[]>(`${ApiRoutes.LISTS}/`);
  }

  getList(id): Observable<List> {
    return this.http.get<List>(`${ApiRoutes.LISTS}/${id}`);
  }

  addList(name): Observable<List> {
    const newList: List = {
      id: this.commonService.generateId(),
      name,
      archived: false
    };
    return this.http.post<List>(`${ApiRoutes.LISTS}/create`, newList);
  }

  renameList(id, name): Observable<List> {
        return this.http.put<List>(`${ApiRoutes.LISTS}/rename/${id}`, name);
  }

  toggleArchive(id: string, state: boolean): Observable<List> {
    return this.http.put<List>(`${ApiRoutes.LISTS}/archive/${id}`, state);
  }

  deleteList(listId): Observable<any> {
    return this.http.delete(`${ApiRoutes.LISTS}/delete/${listId}`);
  }

  constructor(private http: HttpClient,
              private commonService: CommonService) {
  }
}
