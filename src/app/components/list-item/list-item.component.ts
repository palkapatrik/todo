import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from '../../interfaces/task';
import {TaskService} from '../../services/task.service';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.sass']
})
export class ListItemComponent implements OnInit {
  @Input() task: Task;
  @Input() readonly = false;
  @Output() taskDeleted = new EventEmitter<boolean>();

  editActive = false;

  deleteTask(id) {
    this.taskService.removeTask(id)
      .subscribe(data => {
        console.log(`Deleted task with id: ${data.id}`);
        this.taskDeleted.emit(true);
      });
  }

  toggleTaskState() {
    this.taskService.toggleTaskState(this.task, !this.task.completed)
      .subscribe(data => {
        console.log(`Completed task with id: ${data.id}`);
        this.task = data;
      });
  }

  changeTaskValue(newVal) {
    if (newVal !== this.task.value) {
      this.taskService.changeTaskValue(this.task, newVal)
        .subscribe((data: Task) => {
          this.task = data;
          console.log(`Changed task value`);
        });
    }
  }

  constructor(private taskService: TaskService) {
  }

  ngOnInit(): void {
  }

}
