import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {TaskService} from '../../services/task.service';

@Component({
  selector: 'app-new-task',
  templateUrl: './new-task.component.html',
  styleUrls: ['./new-task.component.sass']
})
export class NewTaskComponent implements OnInit {
  @Input() listId: string;
  @Output() taskCreated = new EventEmitter<boolean>();

  taskValueModel: string = null;


  addTask(value: string) {
    console.log('about to add a new task...');
    this.taskService.addTask(value, this.listId)
      .subscribe(
        data => {
          console.log(`Added task: ${JSON.stringify(data)}`);
          this.taskCreated.emit(true);
          this.taskValueModel = '';
        },
        error => console.error(`Adding task failed: ${error}`));
  }

  constructor(private taskService: TaskService) {
  }

  ngOnInit(): void {
  }

}
