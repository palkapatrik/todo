import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MenuItem} from 'primeng/api';

@Component({
  selector: 'app-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.sass']
})
export class NavigationBarComponent implements OnInit {
  @Output() listCreated = new EventEmitter<string>();

  listCreateDialog = false;
  listNameModel: string = null;

  menuItems: MenuItem[];

  openListCreate(): void {
    this.listCreateDialog = true;
  }

  createList(listName: string): void {
    this.listCreated.emit(listName);
    this.listNameModel = '';
    this.listCreateDialog = false;
  }

  constructor() {
  }

  ngOnInit(): void {
    this.menuItems = [
      {
        label: 'New list',
        icon: 'pi pi-fw pi-plus',
        command: () => this.openListCreate()
      },
      {
        label: 'Home',
        icon: 'pi pi-fw pi-home',
        routerLink: '/home',
      },
      {
        label: 'Archived lists',
        icon: 'pi pi-fw pi-inbox',
        routerLink: '/archived',
      }
    ];
  }
}
