import {Component, Input, OnInit} from '@angular/core';
import {List} from '../../interfaces/list';
import {ListService} from '../../services/list.service';

@Component({
  selector: 'app-board',
  templateUrl: './board.component.html',
  styleUrls: ['./board.component.sass']
})
export class BoardComponent implements OnInit {
  @Input() archived = false;
  lists: List[] = [];

  refreshLists() {
    this.listService.getLists(this.archived)
      .subscribe((data: List[]) => this.lists = data);
  }

  createList(listName: string) {
    this.listService.addList(listName)
      .subscribe((data: List) => {
        this.refreshLists();
        console.log(`Created list with name: ${data.name}`);
      });
  }

  constructor(private listService: ListService) { }

  ngOnInit(): void {
    this.refreshLists();
  }

}
