import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ListActions} from '../../enums/list-actions.enum';

@Component({
  selector: 'app-list-action-buttons',
  templateUrl: './list-action-buttons.component.html',
  styleUrls: ['./list-action-buttons.component.sass']
})
export class ListActionButtonsComponent implements OnInit {
  @Input() archived: boolean;
  @Output() event = new EventEmitter<ListActions>();

  actions = ListActions;

  sendEvent(action: ListActions) {
    this.event.emit(action);
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
