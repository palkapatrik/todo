import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-modal-dialog',
  templateUrl: './modal-dialog.component.html',
  styleUrls: ['./modal-dialog.component.sass']
})
export class ModalDialogComponent implements OnInit {
  @Input() dialogHeader: string;
  @Input() displayDialog: boolean;
  @Input() successBtnLabel = 'Save';
  @Output() action = new EventEmitter<boolean>();

  sendAction(val: boolean) {
    this.action.emit(val);
    this.displayDialog = false;
  }

  constructor() {
  }

  ngOnInit(): void {
  }

}
