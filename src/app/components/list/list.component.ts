import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from 'src/app/interfaces/task';
import {TaskService} from '../../services/task.service';
import {List} from '../../interfaces/list';
import {ListService} from '../../services/list.service';
import {ListActions} from '../../enums/list-actions.enum';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.sass']
})
export class ListComponent implements OnInit {
  @Input() list: List;
  @Output() listChanged = new EventEmitter<boolean>();

  tasks: Task[] = [];

  renameDialog = false;
  deletePanel = false;

  listNameModel: string;

  refreshTasks() {
    this.taskService.getTasks(this.list.id)
      .subscribe((data: Task[]) => this.tasks = data);
  }

  toggleArchive(list: List, state: boolean) {
    this.listService.toggleArchive(list.id, state)
      .subscribe((data: List) => {
        console.log(`${state ? 'Archived' : 'Revived'} list with id: ${data.id}`);
        this.listChanged.emit(true);
      });
  }

  deleteList(id) {
    this.listService.deleteList(id)
      .subscribe(() => {
        console.log(`Deleted list with id: ${id}`);
        this.listChanged.emit(true);
      });
  }

  renameList(id, name) {
      this.listService.renameList(id, name)
        .subscribe(data => {
          this.list = data;
          this.renameDialog = false;
          this.listChanged.emit(true);
          console.log(`List renamed to: ${data.name}`);
        });
  }

  openRenameDialog() {
    this.renameDialog = true;
    this.listNameModel = this.list.name;
  }

  handleActionButton(event: ListActions) {
    console.log(`Received action button event.. ${event}`);
    switch (event) {
      case ListActions.ARCHIVE:
        this.toggleArchive(this.list, true);
        break;
      case ListActions.RENAME:
        this.openRenameDialog();
        break;
      case ListActions.DELETE:
        this.deleteList(this.list.id);
        break;
      case ListActions.REVIVE:
        this.toggleArchive(this.list, false);
        break;
    }
  }

  constructor(private taskService: TaskService,
              private listService: ListService) {
  }

  ngOnInit(): void {
    this.refreshTasks();
  }

}
